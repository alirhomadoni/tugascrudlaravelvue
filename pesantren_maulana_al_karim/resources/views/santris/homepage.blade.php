@extends('layouts.app')

@section('content')
<div class="container">
  <div class="jumbotron">
    <div class="text-center">
      <h1>CRUD Laravel and Vue.js</h1>
      <p>Ali Rhomadoni</p>

    </div>
     <router-view></router-view>

  </div>

</div>
@endsection
