<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Santri extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
      'id'            => $this->id,
      'namea'         => $this->nama,
      'tempat lahir'  => $this->tempat_lahir,
      'tanggal_lahir' => $this->tanggal_lahir,
      'asal_daerah'   => $this->asal_daerah,
      'tanggal_masuk' => $this->tanggal_masuk,
      'is_takhosus'   => $this->is_takhosus,
  ];
    }
}
